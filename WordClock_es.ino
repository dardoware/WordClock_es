/************************************************
* Proyecto: WordClock_es
*
* Descripcion: Reloj con palabras (en español)
*
* Autor: Samuel Villafranca Gomez

************************************************/

#include <FastLED.h>
#include <Wire.h>
#include <RTClib.h>
#include <Button.h>

/* Configuracion*/

/* Fin Configuracion*/

#define DIA_ESPECIAL 2
#define MES_ESPECIAL 8

/* Pines Arduino */

#define PIN_MODO_RELOJ     7
#define PIN_MODO_BRILLO    8
#define PIN_LED_CLK2       9
#define PIN_LED_DATA2      10
#define PIN_LED_DATA       11
#define PIN_LED_CLK        13
#define PIN_SENSOR_LUZ     A0
#define PIN_SENSOR_TEMP    A1
#define PIN_CONTROL_BRILLO A2
#define PIN_SELECTOR_COLOR A3

/* FIN pines Arduino */

/* LEDS */

#define CHIPSET  WS2812B

const byte LED_FILAS_Z1 = 2;
const byte LED_COLUMNAS_Z1 = 4;
const byte LED_FILAS_Z2 = 8;
const byte LED_COLUMNAS_Z2 = 12;
const byte NUM_LEDS_Z1 = LED_FILAS_Z1 * LED_COLUMNAS_Z1;
const byte NUM_LEDS_Z2 = LED_FILAS_Z2 * LED_COLUMNAS_Z2;
const int NUM_LEDS = NUM_LEDS_Z1 + NUM_LEDS_Z2;
CRGB leds[NUM_LEDS]; 

#define CHIPSET2 WS2812B
const byte NUM_LEDS2 = 8;
CRGB leds_area2[NUM_LEDS2]; 

byte brillo = 0;
/* FIN LEDS */

byte modoColor = 0;

/* RTC y tiempo */
RTC_DS1307 reloj;

/* FIN RTC y tiempo */


/* CONTROLES */
Button botonColor = Button(PIN_MODO_RELOJ);

/* FIN CONTROLES */

/* COLORES */
CRGB COLOR_ANUNCIO_DEFECTO = CRGB::Green;
CRGB COLOR_HORAS_DEFECTO   = CRGB::Red;
CRGB COLOR_MINUTOS_DEFECTO = CRGB::Blue;

CRGB COLOR_ANUNCIO = COLOR_ANUNCIO_DEFECTO;
CRGB COLOR_HORAS   = COLOR_HORAS_DEFECTO;
CRGB COLOR_MINUTOS = COLOR_MINUTOS_DEFECTO;

CRGB COLOR_FILA0 = CRGB::Orange;
CRGB COLOR_FILA1 = CRGB::Green;
CRGB COLOR_FILA2 = CRGB::Blue;
CRGB COLOR_FILA3 = CRGB::Pink;
CRGB COLOR_FILA4 = CRGB::Yellow;
CRGB COLOR_FILA5 = CRGB::Cyan;
CRGB COLOR_FILA6 = CRGB::Grey;
CRGB COLOR_FILA7 = CRGB::Purple;

CRGB COLOR_MENOS10 = CRGB::LightGrey;
CRGB COLOR_TEMP0   = CRGB::LightBlue;
CRGB COLOR_TEMP10  = CRGB::Cyan;
CRGB COLOR_TEMP15  = CRGB::Green;
CRGB COLOR_TEMP20  = CRGB::Yellow;
CRGB COLOR_TEMP30  = CRGB::Orange;
CRGB COLOR_TEMP40  = CRGB::DarkOrange;
CRGB COLOR_MAS40   = CRGB::Red;

byte TonoEspecial = 66;


/* FIN COLORES */

const byte MODO_AUTOMATICO = 0;
const byte MODO_ARCOIRIS   = 1;
const byte MODO_MANUAL     = 2;

byte colorElegido = 99;

float temp = 0;

void setup() {
  FastLED.addLeds<CHIPSET, PIN_LED_DATA>(leds, NUM_LEDS);
  FastLED.addLeds<CHIPSET2,PIN_LED_DATA2>(leds_area2, NUM_LEDS2);
  Wire.begin();
  pinMode(PIN_MODO_BRILLO, INPUT);
  reloj.begin();
  Serial.begin(9600);
}

void loop() {
  temp = leerSensorTemp();
  if (digitalRead(PIN_MODO_BRILLO==HIGH)) {
    brillo = map(analogRead(PIN_SENSOR_LUZ),0,1023,0,255);
  } else {
    brillo = map(analogRead(PIN_CONTROL_BRILLO),0,1023,0,255);
  }
  
  colorElegido = map(analogRead(PIN_SELECTOR_COLOR),0,1023,0,255);
 
  DateTime ahorita = reloj.now();
  
  if (botonColor.uniquePress()) {
    modoColor++;
  }
  
  debugReloj(ahorita);
  configurarColores();
  pintarReloj(ahorita);
  
  if ((ahorita.day()==DIA_ESPECIAL && ahorita.month()==MES_ESPECIAL)) {
    Serial.print(" hoy es el dia especial ");
       pirotecnia();
  }  
  FastLED.setBrightness(brillo);
  FastLED.show();
  delay(200);
}

float leerSensorTemp(){
  int lectura = analogRead(PIN_SENSOR_TEMP);
  float v = lectura * 5.0;
  float celsius = (v-0.5) * 100;
  return celsius;
}

void apagarArea2() {
  for (int i=0; i< NUM_LEDS2 ;i++) {
    leds_area2[i].CRGB::Yellow;
  }
}

void pirotecnia() {
  apagarArea2();
  for (int i=0; i<NUM_LEDS2; i++) {
    leds_area2[i].CRGB::Blue;
  }
}

void configurarColores() {
   
  switch (modoColor) {
    case 0: // automatico (segun temperatura)
        asignarColorTemperatura();
      break;
    case 1: // normal
      COLOR_ANUNCIO = COLOR_ANUNCIO_DEFECTO;
      COLOR_HORAS   = COLOR_HORAS_DEFECTO;
      COLOR_MINUTOS = COLOR_MINUTOS_DEFECTO;
      break;
    case 2: // manual
      COLOR_ANUNCIO = CHSV(colorElegido,255,255);
      COLOR_HORAS   = CHSV(colorElegido,255,255);
      COLOR_MINUTOS = CHSV(colorElegido,255,255);
      break;
    default:
      modoColor=0;
      break;
  }
}

void asignarColorTemperatura() {
  if (temp <= -10) {
    COLOR_HORAS   = COLOR_MENOS10;
    COLOR_MINUTOS = COLOR_MENOS10;
    COLOR_ANUNCIO = COLOR_MENOS10;
  }
  if (-10 < temp <= 0) {
    COLOR_HORAS   = COLOR_TEMP0;
    COLOR_MINUTOS = COLOR_TEMP0;
    COLOR_ANUNCIO = COLOR_TEMP0;
  }
  if (0 < temp <= 10) {
    COLOR_HORAS   = COLOR_TEMP10;
    COLOR_MINUTOS = COLOR_TEMP10;
    COLOR_ANUNCIO = COLOR_TEMP10;
  }
  if (10 < temp <= 15) {
    COLOR_HORAS   = COLOR_TEMP15;
    COLOR_MINUTOS = COLOR_TEMP15;
    COLOR_ANUNCIO = COLOR_TEMP15;
  }
  if (15 < temp <= 20) {
    COLOR_HORAS   = COLOR_TEMP20;
    COLOR_MINUTOS = COLOR_TEMP20;
    COLOR_ANUNCIO = COLOR_TEMP20;
  }
  if (20 < temp <= 30) {
    COLOR_HORAS   = COLOR_TEMP30;
    COLOR_MINUTOS = COLOR_TEMP30;
    COLOR_ANUNCIO = COLOR_TEMP30;
  }
  if (30 < temp <= 40) {
    COLOR_HORAS   = COLOR_TEMP40;
    COLOR_MINUTOS = COLOR_TEMP40;
    COLOR_ANUNCIO = COLOR_TEMP40;
  }
  if (temp > 40) {
    COLOR_HORAS   = COLOR_MAS40;
    COLOR_MINUTOS = COLOR_MAS40;
    COLOR_ANUNCIO = COLOR_MAS40;
  }

}

void pintarReloj(DateTime t) {
   borrarCuadro();
   pintarHoras(t.hour());
   pintarMinutos(t.minute());
}

void pintarHoras(int hh) {
  
  int h = hh % 12;
  switch (h) {
    case 1:
      horaSingular();
      
      leds[15]= COLOR_HORAS; // U
      leds[14]= COLOR_HORAS; // N
      leds[13]= COLOR_HORAS; // A
      break;
    case 2:
      horaPlural(); 
      leds[16]=COLOR_HORAS; // D
      leds[17]=COLOR_HORAS; // O
      leds[18]=COLOR_HORAS; // S
      break;
    case 3:
      horaPlural();
      leds[38]=COLOR_HORAS; // T
      leds[37]=COLOR_HORAS; // R
      leds[36]=COLOR_HORAS; // E
      leds[35]=COLOR_HORAS; // S
      break;
      
    case 4: 
      horaPlural();   
      leds[19]=COLOR_HORAS; // C
      leds[20]=COLOR_HORAS; // U
      leds[21]=COLOR_HORAS; // A
      leds[22]=COLOR_HORAS; // T
      leds[23]=COLOR_HORAS; // R
      leds[24]=COLOR_HORAS; // O      
      break;
      
    case 5:
      horaPlural();   
      leds[12]=COLOR_HORAS; // C
      leds[11]=COLOR_HORAS; // I
      leds[10]=COLOR_HORAS; // N
      leds[9]=COLOR_HORAS;  // C
      leds[8]=COLOR_HORAS;  // O     
      break;
    
    case 6:  //35..32
      horaPlural();   
      leds[35]=COLOR_HORAS; // S
      leds[34]=COLOR_HORAS; // E
      leds[33]=COLOR_HORAS; // I
      leds[32]=COLOR_HORAS; // S
      break;   
      
    case 7:  //32..28
      horaPlural();   
      leds[32]=COLOR_HORAS; // S
      leds[31]=COLOR_HORAS; // I
      leds[30]=COLOR_HORAS; // E
      leds[29]=COLOR_HORAS; // T
      leds[28]=COLOR_HORAS; // E
      break; 
      
    case 8:  //24..27
      horaPlural();   
      leds[24]=COLOR_HORAS; // O
      leds[25]=COLOR_HORAS; // C
      leds[25]=COLOR_HORAS; // H
      leds[27]=COLOR_HORAS; // O
      break;
      
    case 9:  //63..59
      horaPlural();   
      leds[63]=COLOR_HORAS; // N
      leds[62]=COLOR_HORAS; // U
      leds[61]=COLOR_HORAS; // E
      leds[60]=COLOR_HORAS; // V
      leds[59]=COLOR_HORAS; // E
      break;

    case 10: // 40..43
      horaPlural();   
      leds[40]=COLOR_HORAS; // D
      leds[41]=COLOR_HORAS; // I
      leds[42]=COLOR_HORAS; // E
      leds[43]=COLOR_HORAS; // Z
      break; 
    
    case 11: // 44..47
      horaPlural();   
      leds[44]=COLOR_HORAS; // O
      leds[45]=COLOR_HORAS; // N
      leds[46]=COLOR_HORAS; // C
      leds[47]=COLOR_HORAS; // E
      break;  
     
    case 0: // 48..51
      horaPlural();   
      leds[48]=COLOR_HORAS; // D
      leds[49]=COLOR_HORAS; // O
      leds[50]=COLOR_HORAS; // C
      leds[51]=COLOR_HORAS; // E
      break;       
  }
} 

void pintarMinutos(int mm) {
  int i = mm/5;
  int j = mm%5;
  switch (i) {
    case 0: //00..04;
      leds[64]= COLOR_MINUTOS; // E
      leds[65]= COLOR_MINUTOS; // N
      leds[67]= COLOR_MINUTOS; // P
      leds[68]= COLOR_MINUTOS; // U
      leds[69]= COLOR_MINUTOS; // N
      leds[70]= COLOR_MINUTOS; // T
      leds[71]= COLOR_MINUTOS; // O      
      break;
      
   case 1:
     leds[66]= COLOR_MINUTOS; // Y
     cinco();
     break;
      
    case 2:
      leds[66]= COLOR_MINUTOS; // Y
      diez();
      break;
    case 3:
      leds[66]= COLOR_MINUTOS; // Y
      cuarto();
      break;
    case 4:
      leds[66]= COLOR_MINUTOS; // Y
      veinte();
      break;
    case 5:
      leds[66]= COLOR_MINUTOS; // Y
      veinticinco();
      break;
    case 6:
      leds[66]= COLOR_MINUTOS;  // Y
      leds[111]= COLOR_MINUTOS; // M
      leds[110]= COLOR_MINUTOS; // E
      leds[109]= COLOR_MINUTOS; // D
      leds[108]= COLOR_MINUTOS; // I
      leds[107]= COLOR_MINUTOS; // A
      break;
    case 7:
      menos();
      veinticinco();
      break;
    case 8:
      menos();
      veinte();
      break;
    case 9:
      menos();
      cuarto();
      break;
    case 10:
      menos();
      diez();
      break;
    case 11:
      menos();
      cinco();
      break;
  }
  
  switch(j) {
    case 1: leds[105]= COLOR_MINUTOS; leds[101]= COLOR_MINUTOS;leds[100]= COLOR_MINUTOS; Serial.print("(+1 minutos) "); break;
    case 2: leds[104]= COLOR_MINUTOS; leds[101]= COLOR_MINUTOS;leds[100]= COLOR_MINUTOS; Serial.print("(+2 minutos) "); break;
    case 3: leds[103]= COLOR_MINUTOS; leds[101]= COLOR_MINUTOS;leds[100]= COLOR_MINUTOS; Serial.print("(+3 minutos) "); break;
    case 4: leds[102]= COLOR_MINUTOS; leds[101]= COLOR_MINUTOS;leds[100]= COLOR_MINUTOS; Serial.print("(+4 minutos) "); break;
  }
}

void menos() {
  leds[57]= COLOR_MINUTOS; // M
  leds[56]= COLOR_MINUTOS; // E
  leds[55]= COLOR_MINUTOS; // N
  leds[54]= COLOR_MINUTOS; // O
  leds[53]= COLOR_MINUTOS; // S
}

void cinco() {
  leds[94]= COLOR_MINUTOS; // C
  leds[95]= COLOR_MINUTOS; // I
  leds[96]= COLOR_MINUTOS; // N
  leds[97]= COLOR_MINUTOS; // C
  leds[98]= COLOR_MINUTOS; // O
}

void diez() {
  leds[72]= COLOR_MINUTOS; // D
  leds[73]= COLOR_MINUTOS; // I
  leds[74]= COLOR_MINUTOS; // E
  leds[75]= COLOR_MINUTOS; // Z
}

void cuarto() {
  leds[87]= COLOR_MINUTOS; // C
  leds[86]= COLOR_MINUTOS; // U
  leds[85]= COLOR_MINUTOS; // A
  leds[84]= COLOR_MINUTOS; // R
  leds[83]= COLOR_MINUTOS; // T
  leds[82]= COLOR_MINUTOS; // O
}

void veinte() {
  leds[81]= COLOR_MINUTOS; // V
  leds[80]= COLOR_MINUTOS; // E
  leds[79]= COLOR_MINUTOS; // I
  leds[78]= COLOR_MINUTOS; // N
  leds[77]= COLOR_MINUTOS; // T
  leds[76]= COLOR_MINUTOS; // E
}

void veinticinco() {
  leds[88]= COLOR_MINUTOS; // V
  leds[89]= COLOR_MINUTOS; // E
  leds[90]= COLOR_MINUTOS; // I
  leds[91]= COLOR_MINUTOS; // N
  leds[92]= COLOR_MINUTOS; // T
  leds[93]= COLOR_MINUTOS; // I  
  cinco();                 // CINCO
}


void horaSingular() {
  leds[0]= COLOR_ANUNCIO; // E
  leds[1]= COLOR_ANUNCIO; // S
  leds[5]= COLOR_ANUNCIO; // L
  leds[6]= COLOR_ANUNCIO; // A
}

void horaPlural() {
  leds[1]=COLOR_ANUNCIO; // S
  leds[2]=COLOR_ANUNCIO; // O
  leds[3]=COLOR_ANUNCIO; // N
  leds[5]=COLOR_ANUNCIO; // L
  leds[6]=COLOR_ANUNCIO; // A
  leds[7]=COLOR_ANUNCIO; // S
}

void borrarCuadro() {
  for (byte i=0;i<NUM_LEDS;i++){
    leds[i]=CRGB::Black;
  }
}

void debugReloj(DateTime t) {
  // Fecha (dd/mm/aa)
    Serial.print(t.day(), DEC); 
    Serial.print('/');
    Serial.print(t.month(), DEC);
    Serial.print('/');
    Serial.print(t.year(), DEC);
    Serial.print(' ');
  // Hora (hh/mm/ss)
    Serial.print(t.hour(), DEC);
    Serial.print(':');
    Serial.print(t.minute(), DEC);
    Serial.print(':');
    Serial.print(t.second(), DEC);
    Serial.println();
}


